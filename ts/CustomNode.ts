import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class CustomNode extends Node {
    constructor(cost: number) {
        super(NodeTypeEnum.Custom, cost);
    }

    protected readonly label = "Custom Node";
    protected readonly short = "CN";
}
