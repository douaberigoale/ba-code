import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class ButtonClickNode extends Node {
    constructor() {
        super(NodeTypeEnum.ButtonClickNode);
    }

    protected readonly label = "Button Click Node";
    protected readonly short = "C";

    protected readonly cost: number = 3.73;

}
