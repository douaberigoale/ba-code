import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class TypePasswordNode extends Node {
    constructor() {
        super(NodeTypeEnum.TypePasswordNode);
    }

    protected readonly label = "Type Password Node";
    protected readonly short = "T(P)";

    protected readonly cost: number = 2.24;

}
