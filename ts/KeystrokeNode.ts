import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class KeystrokeNode extends Node {
    constructor() {
        super(NodeTypeEnum.KeystrokeNode);
    }

    protected readonly label = "Keystroke Node";
    protected readonly short = "K";

    protected readonly cost: number = 0.28;

}
