import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class PointingNode extends Node {
    constructor() {
        super(NodeTypeEnum.PointingNode);
    }

    protected readonly label = "Pointing Node";
    protected readonly short = "P";

    protected readonly cost: number = 1.1;

}
