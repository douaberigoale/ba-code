import { INode } from "./Interfaces";
import { NodeTypeEnum } from "./NodeTypeEnum";

export abstract class Node implements INode {
    protected readonly label: string;
    protected readonly short: string;
    protected readonly cost: number;

    protected constructor(
        protected nodeType: NodeTypeEnum,
        protected costInput: number = 0) {
    }

    public getCost(): number {
        if (this.cost !== undefined) {
            return this.cost;
        }
        return this.costInput === undefined ? 0 : this.costInput;
    }

    public getType(): NodeTypeEnum {
        return this.nodeType;
    }

    public getShort(): string {
        return this.short;
    }

    public getLabel(): string {
        return this.label;
    }
}
