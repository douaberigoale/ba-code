import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class StructuralNode extends Node {
    constructor() {
        super(NodeTypeEnum.Structural);
    }

    protected readonly label = "Root Node";
    protected readonly short = "ST";
}
