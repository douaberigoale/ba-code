import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class PerceptionNode extends Node {
    constructor() {
        super(NodeTypeEnum.PerceptionNode);
    }

    protected readonly label = "Perception Node";
    protected readonly short = "M";

    protected readonly cost: number = 1.2;

}
