import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class DropdownNode extends Node {
    constructor() {
        super(NodeTypeEnum.DropdownNode);
    }

    protected readonly label = "Dropdown Node";
    protected readonly short = "P";

    protected readonly cost: number = 3.04;

}
