import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class ScrollingNode extends Node {
    constructor() {
        super(NodeTypeEnum.ScrollingNode);
    }

    protected readonly label = "Scrolling Node";
    protected readonly short = "SC";

    protected readonly cost: number = 3.96;

}
