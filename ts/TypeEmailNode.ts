import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class TypeEmailNode extends Node {
    constructor() {
        super(NodeTypeEnum.TypeEmailNode);
    }

    protected readonly label = "Type Email Node";
    protected readonly short = "T(@)";

    protected readonly cost: number = 7;

}
