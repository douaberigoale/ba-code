import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class MouseButtonNode extends Node {
    constructor() {
        super(NodeTypeEnum.MouseButtonNode);
    }

    protected readonly label = "Mouse Button Node";
    protected readonly short = "B";

    protected readonly cost: number = 0.1;

}
