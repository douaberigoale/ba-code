import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class CutAndPasteNode extends Node {
    constructor() {
        super(NodeTypeEnum.CutAndPasteNode);
    }

    protected readonly label = "Cut And Paste Node";
    protected readonly short = "CP";

    protected readonly cost: number = 4.51;

}
