import { NodeTypeEnum } from "./NodeTypeEnum";

export interface INode {
    getCost(): number;
    getType(): NodeTypeEnum;
    getShort(): string;
}