import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class TypeURLNode extends Node {
    constructor() {
        super(NodeTypeEnum.TypeURLNode);
    }

    protected readonly label = "Type URL Node";
    protected readonly short = "T(W)";

    protected readonly cost: number = 8.4;

}
