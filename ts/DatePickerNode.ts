import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class DatePickerNode extends Node {
    constructor() {
        super(NodeTypeEnum.DatePickerNode);
    }

    protected readonly label = "Date Picker Node";
    protected readonly short = "D";

    protected readonly cost: number = 6.81;

}
