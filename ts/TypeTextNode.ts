import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class TypeTextNode extends Node {
    constructor() {
        super(NodeTypeEnum.TypeTextNode);
    }

    protected readonly label = "Type Text Node";
    protected readonly short = "T";

    protected readonly cost: number = 2.32;

}
