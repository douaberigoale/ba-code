import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class TypeUsername extends Node {
    constructor() {
        super(NodeTypeEnum.TypeUsernameNode);
    }

    protected readonly label = "Type Username";
    protected readonly short = "T(U)";

    protected readonly cost: number = 3.36;

}
