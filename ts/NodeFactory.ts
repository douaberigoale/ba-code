import { INode } from "./Interfaces";
import { UnknownNode } from "./UnknownNode";
import { ButtonClickNode } from "./ButtonClickNode";
import { NodeTypeEnum } from "./NodeTypeEnum";
import { CustomNode } from "./CustomNode";
import { StructuralNode } from "./StructuralNode";
import { DropdownNode } from "./DropdownNode";
import { DatePickerNode } from "./DatePickerNode";
import { TypePasswordNode } from "./TypePasswordNode";
import { TypeEmailNode } from "./TypeEmailNode";
import { TypeUsername } from "./TypeUsername";
import { ScrollingNode } from "./ScrollingNode";
import { TypeTextNode } from "./TypeTextNode";
import { KeystrokeNode } from "./KeystrokeNode";
import { PointingNode } from "./PointingNode";
import { MouseButtonNode } from "./MouseButtonNode";
import { HomingNode } from "./HomingNode";
import { PerceptionNode } from "./PerceptionNode";
import { TypeURLNode } from "./TypeURLNode";
import { CutAndPasteNode } from "./CutAndPasteNode";

export class NodeFactory {
    public createNodeFromCell(cell): INode {
        return this.createNodeByType(
            cell.getAttribute("nodeType"),
            cell.getAttribute("klmCost"));
    }

    public createNodeByType(type, cost = 0): INode {
        switch (parseInt(type)) {
            case NodeTypeEnum.Custom: {
                return new CustomNode(cost);
            }
            case NodeTypeEnum.ButtonClickNode: return new ButtonClickNode();
            case NodeTypeEnum.Structural: return new StructuralNode();
            case NodeTypeEnum.DropdownNode: return new DropdownNode();
            case NodeTypeEnum.DatePickerNode: return new DatePickerNode();
            case NodeTypeEnum.TypePasswordNode: return new TypePasswordNode();
            case NodeTypeEnum.TypeEmailNode: return new TypeEmailNode();
            case NodeTypeEnum.TypeUsernameNode: return new TypeUsername();
            case NodeTypeEnum.ScrollingNode: return new ScrollingNode();
            case NodeTypeEnum.TypeTextNode: return new TypeTextNode();
            case NodeTypeEnum.KeystrokeNode: return new KeystrokeNode();
            case NodeTypeEnum.PointingNode: return new PointingNode();
            case NodeTypeEnum.MouseButtonNode: return new MouseButtonNode();
            case NodeTypeEnum.HomingNode: return new HomingNode();
            case NodeTypeEnum.PerceptionNode: return new PerceptionNode();
            case NodeTypeEnum.TypeURLNode: return new TypeURLNode();
            case NodeTypeEnum.CutAndPasteNode: return new CutAndPasteNode();

            default: return new UnknownNode();
        }
    }
}