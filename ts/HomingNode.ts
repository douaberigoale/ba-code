import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class HomingNode extends Node {
    constructor() {
        super(NodeTypeEnum.HomingNode);
    }

    protected readonly label = "Homing Node";
    protected readonly short = "H";

    protected readonly cost: number = 0.4;

}
