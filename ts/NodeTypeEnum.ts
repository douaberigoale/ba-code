export enum NodeTypeEnum {
    Structural = -1,
    Unknown = 0,
    Custom = 1,
    ButtonClickNode = 2,
    DropdownNode = 3,
    DatePickerNode = 4,
    TypePasswordNode = 5,
    TypeEmailNode = 6,
    TypeUsernameNode = 7,
    ScrollingNode = 8,
    TypeTextNode = 9,
    KeystrokeNode = 10,
    PointingNode = 11,
    MouseButtonNode = 12,
    HomingNode = 13,
    PerceptionNode = 14,
    TypeURLNode = 15,
    CutAndPasteNode = 16
}

(window as { NodeTypeEnum?}).NodeTypeEnum = NodeTypeEnum;

