import { Node } from "./Node";
import { NodeTypeEnum } from "./NodeTypeEnum";

export class UnknownNode extends Node {
    constructor() {
        super(NodeTypeEnum.Unknown);
    }

    protected readonly label = "Unknown Node";
    protected readonly short = "Unknown";

    protected readonly cost: number = 0;

}
