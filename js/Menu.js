/**
 * Constructs a new action for the given parameters.
 */
function Menu(funct, enabled) {
    mxEventSource.call(this);
    this.funct = funct;
    this.enabled = enabled != null ? enabled : true;
}
// Menu inherits from mxEventSource
mxUtils.extend(Menu, mxEventSource);
/**
 * Sets the enabled state of the action and fires a stateChanged event.
 */
Menu.prototype.isEnabled = function () {
    return this.enabled;
};
/**
 * Sets the enabled state of the action and fires a stateChanged event.
 */
Menu.prototype.setEnabled = function (value) {
    if (this.enabled != value) {
        this.enabled = value;
        this.fireEvent(new mxEventObject("stateChanged"));
    }
};
/**
 * Sets the enabled state of the action and fires a stateChanged event.
 */
Menu.prototype.execute = function (menu, parent) {
    this.funct(menu, parent);
};
/**
 * "Installs" menus in EditorUi.
 */
EditorUi.prototype.createMenus = function () {
    return new Menus(this);
};