/**
 * Adds the label menu items to the given menu and parent.
 */
StyleFormatPanel = function (format, editorUi, container) {
  BaseFormatPanel.call(this, format, editorUi, container);
  this.init();
};
mxUtils.extend(StyleFormatPanel, BaseFormatPanel);

StyleFormatPanel.prototype.init = function () {
  var ui = this.editorUi;
  var graph = ui.editor.graph;
  var selectedCells = graph.getSelectionCells();

  if (selectedCells.length == 1) {
    this.container.appendChild(this.addProperties(this.createPanel()));
  }
  this.container.appendChild(this.addChart(this.createPanel()));
  this.container.appendChild(this.addLegend(this.createPanel()));
};

StyleFormatPanel.prototype.addLegend = function (container) {
  var tbl = document.createElement("table");
  tbl.style.width = '100%';
  tbl.style.border = "1px solid";
  tbl.style.padding = "15px";

  var tbdy = document.createElement('tbody');
  var tr = document.createElement('tr');
  var td = document.createElement('td');
  var td2 = document.createElement('td');
  var td3 = document.createElement('td');

  td.appendChild(document.createTextNode("Operator"));
  td2.appendChild(document.createTextNode("Description"));
  td3.appendChild(document.createTextNode("Cost (seconds)"));

  td.style.borderBottom = "1px solid";
  td2.style.borderBottom = "1px solid";
  td3.style.borderBottom = "1px solid";

  tbdy.setAttribute('border', '1');


  tr.appendChild(td);
  tr.appendChild(td2);
  tr.appendChild(td3);
  tr.style.border = "1px solid";

  tbdy.appendChild(tr);

  var operators = ["K", "P", "B", "H", "M", "C", "P", "D", "CP", "SC", "T", "T(@)", "T(P)", "T(U)", "T(W)"];
  var description = ["Keystroke", "Pointing", "Mouse Button Press", "Homing", "Perception", "Button Click", "Dropdown List", "Datepicker", "Cut&Paste", "Scrolling", "Type Text", "Type E-Mail Adress", "Type Password", "Type Username", "Type URL"];
  var costs = [0.28, 1.1, 0.1, 0.4, 1.2, 3.37, 3.04, 6.81, 4.51, 3.96, 2.32, 7, 2.24, 3.36, 8.4];

  for (var i = 0; i < operators.length; i++) {
    var tr = document.createElement('tr');
    var td = document.createElement('td');
    td.appendChild(document.createTextNode(operators[i]));

    var tdCost = document.createElement('td');
    tdCost.appendChild(document.createTextNode(description[i]));

    var tdStatusQuo = document.createElement('td');
    tdStatusQuo.appendChild(document.createTextNode(costs[i]));

    tr.appendChild(td);
    tr.appendChild(tdCost);
    tr.appendChild(tdStatusQuo);

    tbdy.appendChild(tr);
  }
  tbl.appendChild(tbdy);
  container.appendChild(tbl);

  return container;
};

StyleFormatPanel.prototype.addChart = function (container) {
  var div = document.createElement("div");

  var canvas = document.createElement("canvas");
  canvas.id = "line-chart";
  canvas.width = 90;
  canvas.height = 50;

  var cumulativeCanvas = document.createElement("canvas");
  cumulativeCanvas.id = "cumulative-chart";
  cumulativeCanvas.width = 90;
  cumulativeCanvas.height = 50;

  var form = new mxForm();
  var formForStatusQuo = new mxForm();

  function createDropdownForChart(graph, form, roots, selected) {
    var combo = form.addCombo("Chart: ", false, 0);

    var selectionShouldBeReset = selected === undefined;

    form.addOption(combo, "All", 0, selectionShouldBeReset);

    roots.forEach(root => {
      form.addOption(combo, root.getAttribute("label"), root.id, parseInt(root.getAttribute("chartSelected")) === 1);
    });

    var applyHandler = function () {
      var newValue = combo.value;
      var oldValue = selected !== undefined ? selected.id : 0;

      if (newValue != oldValue) {
        graph.getModel().beginUpdate();

        try {
          var setNewValue = new mxCellAttributeChange(
            graph.getModel().getCell(newValue), "chartSelected",
            1);
          var resetPreviousValue = new mxCellAttributeChange(
            graph.getModel().getCell(oldValue), "chartSelected",
            0);

          graph.getModel().execute(setNewValue);
          graph.getModel().execute(resetPreviousValue);

        } finally {

          graph.getModel().endUpdate();
        }
      }
    };

    mxEvent.addListener(combo, 'keypress', function (evt) {
      if (evt.keyCode == /*enter*/ 13 &&
        !mxEvent.isShiftDown(evt)) {
        combo.blur();
      }
    });

    if (mxClient.IS_IE) {
      mxEvent.addListener(combo, 'focusout', applyHandler);
    } else {
      mxEvent.addListener(combo, 'blur', applyHandler);
    }
  }

  function createDropdownForStatusQuo(graph, form, roots, selected) {
    var combo = form.addCombo("Status Quo: ", false, 0);

    roots.forEach(root => {
      form.addOption(combo, root.getAttribute("label"), root.id, parseInt(root.getAttribute("statusQuo")) === 1);
    });

    var applyHandler = function () {
      var newValue = combo.value;
      var oldValue = selected !== undefined ? selected.id : 0;

      if (newValue != oldValue) {
        graph.getModel().beginUpdate();

        try {
          var setNewValue = new mxCellAttributeChange(
            graph.getModel().getCell(newValue), "statusQuo",
            1);
          var resetPreviousValue = new mxCellAttributeChange(
            graph.getModel().getCell(oldValue), "statusQuo",
            0);

          graph.getModel().execute(setNewValue);
          graph.getModel().execute(resetPreviousValue);

        } finally {

          graph.getModel().endUpdate();
        }
      }
    };

    mxEvent.addListener(combo, 'keypress', function (evt) {
      if (evt.keyCode == /*enter*/ 13 &&
        !mxEvent.isShiftDown(evt)) {
        combo.blur();
      }
    });

    if (mxClient.IS_IE) {
      mxEvent.addListener(combo, 'focusout', applyHandler);
    } else {
      mxEvent.addListener(combo, 'blur', applyHandler);
    }
  }

  var cells = this.editorUi.editor.graph.model.cells;

  var roots = [];
  var totalCost;
  var selected;
  var selectedForStatusQuo;
  var factory = new KLM.NodeFactory();

  if (cells !== null && cells !== undefined) {
    cells.forEach(cell => {
      if (cell !== undefined && cell !== null) {
        var node = factory.createNodeFromCell(cell);
        if (node.getType() === window.NodeTypeEnum.Structural)
          roots.push(cell);
      }

      if (cell.getAttribute("chartSelected") !== null &&
        parseInt(cell.getAttribute("chartSelected")) === 1) {
        selected = cell;
      }
      if (cell.getAttribute("statusQuo") !== null &&
        parseInt(cell.getAttribute("statusQuo")) === 1) {
        selectedForStatusQuo = cell;
      }
    });

    createDropdownForChart(this.editorUi.editor.graph, form, roots, selected);
    createDropdownForStatusQuo(this.editorUi.editor.graph, formForStatusQuo, roots, selectedForStatusQuo);

    if (selected !== undefined) {
      drawGraphForSelected(this.editorUi.editor.graph, canvas, selected);
    } else {
      drawLineGraph(canvas, roots);
      drawCumulatedGraph(cumulativeCanvas, roots);
    }
  }

  function drawGraphForSelected(graph, canvas, root) {
    var factory = new KLM.NodeFactory();
    var children = [];
    var costs = [];
    var cumulatedCosts = [];
    var labels = [];

    function getChildrenRecursive(cell) {
      if (cell.getChildCount() === 0) {
        return;
      } else {
        cell.children.forEach(child => {
          if (parseInt(child.edge) !== 1 && child.edge === false && child.getAttribute('chartEnabled') === "true") {
            children.push(child);
            getChildrenRecursive(child);
          }
        });
      }
    }

    children.push(root);
    getChildrenRecursive(root);

    children.forEach(datapoint => {
      var node = factory.createNodeFromCell(datapoint);

      costs.push(node.getCost());
      labels.push(datapoint.getAttribute("label"));

      if (cumulatedCosts === undefined || cumulatedCosts.length == 0) {
        cumulatedCosts.push(node.getCost());
      } else {
        cumulatedCosts.push((
          parseFloat(cumulatedCosts[cumulatedCosts.length - 1]) +
          parseFloat(node.getCost())).toFixed(2));
      }
    });

    totalCost = cumulatedCosts[cumulatedCosts.length - 1];

    var config = {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: 'Action Costs',
          backgroundColor: "rgba(255, 0, 0, 0.3)",
          borderColor: "rgba(255, 0, 0, 0.3)",
          data: costs,
          fill: false,
          showLine: false
        }, {
          label: 'Cumulated Costs',
          fill: true,
          backgroundColor: "rgba(0, 0, 255, 0.1)",
          borderColor: "rgba(0, 0, 255, 0.1)",
          data: cumulatedCosts,
        }]
      },
      options: {
        responsive: true,
        title: {
          display: false,
          text: ''
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: false,
            scaleLabel: {
              display: true,
              labelString: 'Node'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Cost'
            }
          }]
        },
        layout: {
          padding: {
            left: 0,
            right: 30,
            top: 0,
            bottom: 0
          }
        },
        onClick: function (evt) {
          var element = this.getElementAtEvent(evt);
          if (element.length > 0) {
            var index = element[0]._index;
            var chosenCellOnChart = children[index];
            var highlight = new mxCellHighlight(graph, '#ff0000', 3, true);
            highlight.highlight(graph.view.getState(chosenCellOnChart));

            setTimeout(function () {
              highlight.destroy();
            }, 3000);

          }
        }

      }
    };

    new Chart(canvas, config);
  }

  div.appendChild(formForStatusQuo.table);
  div.appendChild(form.table);
  div.appendChild(canvas);

  if (selected === undefined) {
    div.appendChild(cumulativeCanvas);
  } else {
    div.appendChild(createTotalCostElement(totalCost));
  }

  container.appendChild(div);

  return container;
};


function drawLineGraph(canvas, roots) {
  var factory = new KLM.NodeFactory();
  var colours = ["rgba(255, 0, 0, 0.3)", "rgba(0, 255, 0, 0.3)", "rgba(0, 0, 255, 0.3)"];

  var config = {
    type: 'scatter',
    data: {
      datasets: []
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Action Chart for all Graphs'
      },
      tooltips: {},
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Actions'
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Action Costs'
          },
          ticks: {
            min: 0
          }
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 30,
          top: 0,
          bottom: 0
        }
      }
    }
  };

  function getChildrenRecursive(cell) {
    if (cell === undefined || cell.getChildCount() === 0) {
      return;
    } else {
      cell.children.forEach(child => {
        if (parseInt(child.edge) !== 1 && child.edge === false && child.getAttribute('chartEnabled') === "true") {
          children.push(child);
          getChildrenRecursive(child);
        }
      });
    }
  }

  for (var i = 0; i < roots.length; i++) {
    var children = [];
    var labels = [];
    var costs = [];

    children.push(roots[i]);
    getChildrenRecursive(roots[i]);

    var count = 0;
    children.forEach(
      datapoint => {
        var node = factory.createNodeFromCell(datapoint);
        costs.push({
          x: count,
          y: node.getCost()
        });
        count++;
        labels.push(datapoint.getAttribute("label"));
      });

    var data = {
      backgroundColor: colours[colours.length % i],
      borderColor: colours[colours.length % i],
      data: costs,
      showLine: false,
      label: roots[i].getAttribute("label"),
      labels: labels,
      fill: false,
    };

    config.data.datasets.push(data);
  }
  new Chart(canvas, config);
}

function drawCumulatedGraph(cumulativeCanvas, roots) {
  var factory = new KLM.NodeFactory();
  var colours = ["rgba(255, 0, 0, 0.3)", "rgba(0, 255, 0, 0.3)", "rgba(0, 0, 255, 0.3)"];

  var cumulatedConfig = {
    type: 'scatter',
    data: {
      datasets: []
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Cumulated Chart for all Graphs '
      },
      tooltips: {},
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Actions'
          },
          ticks: {
            min: 0
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Cumulated Costs'
          },
          ticks: {
            min: 0
          }
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 30,
          top: 0,
          bottom: 0
        }
      }
    }
  };

  function getChildrenRecursive(cell) {
    if (cell === undefined || cell.getChildCount() === 0) {
      return;
    } else {
      cell.children.forEach(child => {
        if (parseInt(child.edge) !== 1 && child.edge === false && child.getAttribute('chartEnabled') === "true") {
          children.push(child);
          getChildrenRecursive(child);
        }
      });
    }
  }

  for (var i = 0; i < roots.length; i++) {
    var children = [];
    var labels = [];
    var costs = [];

    children.push(roots[i]);
    getChildrenRecursive(roots[i]);

    var count = 0;
    children.forEach(datapoint => {
      var node = factory.createNodeFromCell(datapoint);
      if (costs === undefined || costs.length == 0) {
        costs.push({
          x: count,
          y: node.getCost()
        });
      } else {
        costs.push({
          x: count,
          y: (parseFloat(costs[costs.length - 1].y) + parseFloat(node.getCost())).toFixed(2)
        });
      }
      count++;
    });

    var cumulatedData = {
      backgroundColor: colours[colours.length % i],
      borderColor: colours[colours.length % i],
      data: costs,
      showLine: true,
      label: roots[i].getAttribute("label") + " Cumulated",
      labels: labels,
      fill: false,
    };

    cumulatedConfig.data.datasets.push(cumulatedData);
  }
  new Chart(cumulativeCanvas, cumulatedConfig);
}

function createTotalCostElement(number) {
  var div = document.createElement("div");
  div.style.padding = "6px 0px 1px 0px";
  div.style.whiteSpace = "nowrap";
  div.style.overflow = "hidden";
  div.style.width = "200px";
  div.style.height = (mxClient.IS_QUIRKS) ? "27px" : "18px";

  var textNodeDiv = document.createElement("div");
  textNodeDiv.style.cssFloat = "right";
  var textNode = document.createTextNode(number);
  textNodeDiv.appendChild(textNode);

  var span = document.createElement("span");
  mxUtils.write(span, "Total Cost:");

  div.appendChild(span);
  div.appendChild(textNode);
  return div;
}

StyleFormatPanel.prototype.addProperties = function (container) {
  var ui = this.editorUi;
  var graph = ui.editor.graph;
  var selectedCells = graph.getSelectionCells();

  value = graph.getModel().getValue(selectedCells[0]);

  // Creates the dialog contents
  var form = new mxForm();

  var addTextArea = function (cell, attribute) {
    var oldValue = cell.getAttribute(attribute, '');

    var input = form.addText(attribute + ':', oldValue);

    var applyHandler = function () {
      var newValue = input.value || '';
      var oldValue = cell.getAttribute(attribute, '');

      if (newValue != oldValue) {
        graph.getModel().beginUpdate();

        try {
          var edit = new mxCellAttributeChange(
            cell, attribute,
            newValue);
          graph.getModel().execute(edit);
        } finally {
          graph.getModel().endUpdate();
        }
      }
    };

    mxEvent.addListener(input, 'keypress', function (evt) {
      if (evt.keyCode == /*enter*/ 13 &&
        !mxEvent.isShiftDown(evt)) {
        input.blur();
      }
    });

    if (mxClient.IS_IE) {
      mxEvent.addListener(input, 'focusout', applyHandler);
    } else {
      mxEvent.addListener(input, 'blur', applyHandler);
    }
  };
  var addCombo = function () {
    // todo make schön
    var combo = form.addCombo("nodeType", false, 1);
    form.addOption(combo, "Unknown", parseInt(window.NodeTypeEnum.Unknown), parseInt(nodeType) == window.NodeTypeEnum.Unknown);
    form.addOption(combo, "Structural Node", parseInt(window.NodeTypeEnum.Structural), parseInt(nodeType) == window.NodeTypeEnum.Structural);
    form.addOption(combo, "Custom", parseInt(window.NodeTypeEnum.Custom), parseInt(nodeType) == window.NodeTypeEnum.Custom);
    form.addOption(combo, "Mouse Button Click", parseInt(window.NodeTypeEnum.ButtonClickNode), parseInt(nodeType) == window.NodeTypeEnum.ButtonClickNode);
    form.addOption(combo, "Dropdown Node", parseInt(window.NodeTypeEnum.DropdownNode), parseInt(nodeType) == window.NodeTypeEnum.DropdownNode);
    form.addOption(combo, "DatePicker Node", parseInt(window.NodeTypeEnum.DatePickerNode), parseInt(nodeType) == window.NodeTypeEnum.DatePickerNode);
    form.addOption(combo, "TypePassword Node", parseInt(window.NodeTypeEnum.TypePasswordNode), parseInt(nodeType) == window.NodeTypeEnum.TypePasswordNode);
    form.addOption(combo, "TypeEmail Node", parseInt(window.NodeTypeEnum.TypeEmailNode), parseInt(nodeType) == window.NodeTypeEnum.TypeEmailNode);
    form.addOption(combo, "TypeUsername Node", parseInt(window.NodeTypeEnum.TypeUsernameNode), parseInt(nodeType) == window.NodeTypeEnum.TypeUsernameNode);
    form.addOption(combo, "Scrolling Node", parseInt(window.NodeTypeEnum.ScrollingNode), parseInt(nodeType) == window.NodeTypeEnum.ScrollingNode);

    form.addOption(combo, "TypeText Node", parseInt(window.NodeTypeEnum.TypeTextNode), parseInt(nodeType) == window.NodeTypeEnum.TypeTextNode);
    form.addOption(combo, "Keystroke Node", parseInt(window.NodeTypeEnum.KeystrokeNode), parseInt(nodeType) == window.NodeTypeEnum.KeystrokeNode);
    form.addOption(combo, "Pointing Node", parseInt(window.NodeTypeEnum.PointingNode), parseInt(nodeType) == window.NodeTypeEnum.PointingNode);
    form.addOption(combo, "MouseButton Node", parseInt(window.NodeTypeEnum.MouseButtonNode), parseInt(nodeType) == window.NodeTypeEnum.MouseButtonNode);
    form.addOption(combo, "Homing Node", parseInt(window.NodeTypeEnum.HomingNode), parseInt(nodeType) == window.NodeTypeEnum.HomingNode);
    form.addOption(combo, "Perception Node", parseInt(window.NodeTypeEnum.PerceptionNode), parseInt(nodeType) == window.NodeTypeEnum.PerceptionNode);
    form.addOption(combo, "TypeURL Node", parseInt(window.NodeTypeEnum.TypeURLNode), parseInt(nodeType) == window.NodeTypeEnum.TypeURLNode);
    form.addOption(combo, "CutAndPaste Node", parseInt(window.NodeTypeEnum.CutAndPasteNode), parseInt(nodeType) == window.NodeTypeEnum.CutAndPasteNode);



    var applyHandler = function () {
      var newValue = combo.value || '';
      var oldValue = selectedCells[0].getAttribute("nodeType", '');

      if (newValue != oldValue) {
        graph.getModel().beginUpdate();

        try {
          var edit = new mxCellAttributeChange(
            selectedCells[0], "nodeType",
            newValue);
          graph.getModel().execute(edit);
        } finally {
          graph.getModel().endUpdate();
        }
      }
    };

    mxEvent.addListener(combo, 'keypress', function (evt) {
      if (evt.keyCode == /*enter*/ 13 &&
        !mxEvent.isShiftDown(evt)) {
        combo.blur();
      }
    });

    if (mxClient.IS_IE) {
      mxEvent.addListener(combo, 'focusout', applyHandler);
    } else {
      mxEvent.addListener(combo, 'blur', applyHandler);
    }
  };

  var fieldList = [];
  var cost = value.getAttribute("klmCost");
  var nodeType = value.getAttribute("nodeType");
  var label = value.getAttribute("label");

  if (parseInt(nodeType) === window.NodeTypeEnum.Custom) {
    fieldList.push({
      name: "Cost",
      key: "klmCost",
      value: cost
    });
  }

  fieldList.push({
    name: "Label",
    key: "label",
    value: label
  });

  addCombo();

  for (var i = 0; i < fieldList.length; i++) {
    if (fieldList[i].key !== "nodeType") {
      addTextArea(selectedCells[0], fieldList[i].key);
    }
  }

  var top = document.createElement('div');
  top.appendChild(form.table);

  var newProp = document.createElement('div');
  newProp.style.whiteSpace = 'nowrap';
  newProp.style.marginTop = '6px';

  top.appendChild(newProp);
  container.appendChild(top);
  return container;
};

StyleFormatPanel.prototype.getCustomColors = function () {
  var ss = this.format.getSelectionState();
  var result = [];
  if (ss.style.shape == "swimlane") {
    result.push({
      title: mxResources.get("laneColor"),
      key: "swimlaneFillColor",
      defaultValue: "#ffffff"
    });
  }
  return result;
};