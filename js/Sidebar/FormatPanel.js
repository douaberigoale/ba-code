/**
 * Copyright (c) 2006-2012, JGraph Ltd
 */
Format = function(editorUi, container) {
  this.editorUi = editorUi;
  this.container = container;
};

/**
 * Returns information about the current selection.
 */
Format.prototype.labelIndex = 0;

/**
 * Returns information about the current selection.
 */
Format.prototype.currentIndex = 0;
/**
 * Background color for inactive tabs.
 */
Format.prototype.inactiveTabBackgroundColor = "#f1f3f4";

/**
 * Background color for inactive tabs.
 */
Format.prototype.roundableShapes = [
  "label",
  "rectangle",
  "internalStorage",
  "corner",
  "parallelogram",
  "swimlane",
  "triangle",
  "trapezoid",
  "ext",
  "step",
  "tee",
  "process",
  "link",
  "rhombus",
  "offPageConnector",
  "loopLimit",
  "hexagon",
  "manualInput",
  "curlyBracket",
  "singleArrow",
  "callout",
  "doubleArrow",
  "flexArrow",
  "card",
  "umlLifeline"
];

/**
 * Adds the label menu items to the given menu and parent.
 */
Format.prototype.init = function() {
  var ui = this.editorUi;
  var editor = ui.editor;
  var graph = editor.graph;

  this.update = mxUtils.bind(this, function(sender, evt) {
    this.clearSelectionState();
    this.refresh();
  });

  graph.getSelectionModel().addListener(mxEvent.CHANGE, this.update);
  graph.addListener(mxEvent.EDITING_STARTED, this.update);
  graph.addListener(mxEvent.EDITING_STOPPED, this.update);
  graph.getModel().addListener(mxEvent.CHANGE, this.update);
  graph.addListener(
    mxEvent.ROOT,
    mxUtils.bind(this, function() {
      this.refresh();
    })
  );

  editor.addListener(
    "autosaveChanged",
    mxUtils.bind(this, function() {
      this.refresh();
    })
  );

  this.refresh();
};

/**
 * Returns information about the current selection.
 */
Format.prototype.clearSelectionState = function() {
  this.selectionState = null;
};

/**
 * Returns information about the current selection.
 */
Format.prototype.getSelectionState = function() {
  if (this.selectionState == null) {
    this.selectionState = this.createSelectionState();
  }

  return this.selectionState;
};

/**
 * Returns information about the current selection.
 */
Format.prototype.createSelectionState = function() {
  var cells = this.editorUi.editor.graph.getSelectionCells();
  var result = this.initSelectionState();

  for (var i = 0; i < cells.length; i++) {
    this.updateSelectionStateForCell(result, cells[i], cells);
  }

  return result;
};

/**
 * Returns information about the current selection.
 */
Format.prototype.initSelectionState = function() {
  return {
    vertices: [],
    edges: [],
    x: null,
    y: null,
    style: {},
    fill: true
  };
};

/**
 * Returns information about the current selection.
 */
Format.prototype.updateSelectionStateForCell = function(result, cell, cells) {
  var graph = this.editorUi.editor.graph;

  if (graph.getModel().isVertex(cell)) {
    result.vertices.push(cell);
    var geo = graph.getCellGeometry(cell);

    if (geo != null) {
      if (geo.width > 0) {
        if (result.width == null) {
          result.width = geo.width;
        } else if (result.width != geo.width) {
          result.width = "";
        }
      } else {
        result.containsLabel = true;
      }

      if (geo.height > 0) {
        if (result.height == null) {
          result.height = geo.height;
        } else if (result.height != geo.height) {
          result.height = "";
        }
      } else {
        result.containsLabel = true;
      }

      if (!geo.relative || geo.offset != null) {
        var x = geo.relative ? geo.offset.x : geo.x;
        var y = geo.relative ? geo.offset.y : geo.y;

        if (result.x == null) {
          result.x = x;
        } else if (result.x != x) {
          result.x = "";
        }

        if (result.y == null) {
          result.y = y;
        } else if (result.y != y) {
          result.y = "";
        }
      }
    }
  } else if (graph.getModel().isEdge(cell)) {
    result.edges.push(cell);
  }

  var state = graph.view.getState(cell);

  if (state != null) {
    result.fill = result.fill && this.isFillState(state);

    var shape = mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null);
    result.containsImage = result.containsImage || shape == "image";

    for (var key in state.style) {
      var value = state.style[key];

      if (value != null) {
        if (result.style[key] == null) {
          result.style[key] = value;
        } else if (result.style[key] != value) {
          result.style[key] = "";
        }
      }
    }
  }
};

/**
 * Returns information about the current selection.
 */
Format.prototype.isFillState = function(state) {
  return (
    state.view.graph.model.isVertex(state.cell) ||
    mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null) == "arrow" ||
    mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null) ==
      "filledEdge" ||
    mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null) == "flexArrow"
  );
};

/**
 * Returns information about the current selection.
 */
Format.prototype.isAutoSizeState = function(state) {
  return mxUtils.getValue(state.style, mxConstants.STYLE_AUTOSIZE, null) == "1";
};

/**
 * Returns information about the current selection.
 */
Format.prototype.isImageState = function(state) {
  var shape = mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null);

  return shape == "label" || shape == "image";
};

/**
 * Returns information about the current selection.
 */
Format.prototype.isShadowState = function(state) {
  var shape = mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null);

  return shape != "image";
};

/**
 * Adds the label menu items to the given menu and parent.
 */
Format.prototype.clear = function() {
  this.container.innerHTML = "";

  // Destroy existing panels
  if (this.panels != null) {
    for (var i = 0; i < this.panels.length; i++) {
      this.panels[i].destroy();
    }
  }

  this.panels = [];
};

/**
 * Adds the label menu items to the given menu and parent.
 */
Format.prototype.refresh = function() {
  // Performance tweak: No refresh needed if not visible
  if (this.container.style.width == "0px") {
    return;
  }

  this.clear();
  var ui = this.editorUi;
  var graph = ui.editor.graph;

  var div = document.createElement("div");
  div.style.whiteSpace = "nowrap";
  div.style.color = "rgb(112, 112, 112)";
  div.style.textAlign = "left";
  div.style.cursor = "default";

  var label = document.createElement("div");
  label.className = "geFormatSection";
  label.style.textAlign = "center";
  label.style.fontWeight = "bold";
  label.style.paddingTop = "8px";
  label.style.fontSize = "13px";
  label.style.borderWidth = "0px 0px 1px 1px";
  label.style.borderStyle = "solid";
  label.style.display = mxClient.IS_QUIRKS ? "inline" : "inline-block";
  label.style.height = mxClient.IS_QUIRKS ? "34px" : "25px";
  label.style.overflow = "hidden";
  label.style.width = "100%";
  this.container.appendChild(div);

  // Prevents text selection
  mxEvent.addListener(
    label,
    mxClient.IS_POINTER ? "pointerdown" : "mousedown",
    mxUtils.bind(this, function(evt) {
      evt.preventDefault();
    })
  );

  var containsLabel = this.getSelectionState().containsLabel;
  var currentLabel = null;
  var currentPanel = null;

  var addClickHandler = mxUtils.bind(this, function(elt, panel, index) {
    var clickHandler = mxUtils.bind(this, function(evt) {
      if (currentLabel != elt) {
        if (containsLabel) {
          this.labelIndex = index;
        } else {
          this.currentIndex = index;
        }

        if (currentLabel != null) {
          currentLabel.style.backgroundColor = this.inactiveTabBackgroundColor;
          currentLabel.style.borderBottomWidth = "1px";
        }

        currentLabel = elt;
        currentLabel.style.backgroundColor = "";
        currentLabel.style.borderBottomWidth = "0px";

        if (currentPanel != panel) {
          if (currentPanel != null) {
            currentPanel.style.display = "none";
          }

          currentPanel = panel;
          currentPanel.style.display = "";
        }
      }
    });

    mxEvent.addListener(elt, "click", clickHandler);

    // Prevents text selection
    mxEvent.addListener(
      elt,
      mxClient.IS_POINTER ? "pointerdown" : "mousedown",
      mxUtils.bind(this, function(evt) {
        evt.preventDefault();
      })
    );

    if (index == (containsLabel ? this.labelIndex : this.currentIndex)) {
      // Invokes handler directly as a workaround for no click on DIV in KHTML.
      clickHandler();
    }
  });

  var idx = 0;

  label.style.backgroundColor = this.inactiveTabBackgroundColor;
  label.style.borderLeftWidth = "1px";
  label.style.cursor = "pointer";
  label.style.width = containsLabel ? "50%" : "33.3%";
  label.style.width = containsLabel ? "50%" : "33.3%";
  var label2 = label.cloneNode(false);
  var label3 = label2.cloneNode(false);

  // Workaround for ignored background in IE
  label2.style.backgroundColor = this.inactiveTabBackgroundColor;
  label3.style.backgroundColor = this.inactiveTabBackgroundColor;

  // Style
  if (containsLabel) {
    label2.style.borderLeftWidth = "0px";
  } else {
    label.style.borderLeftWidth = "0px";
    mxUtils.write(label, "Properties");
    div.appendChild(label);

    var stylePanel = div.cloneNode(false);
    stylePanel.style.display = "none";
    this.panels.push(new StyleFormatPanel(this, ui, stylePanel));
    this.container.appendChild(stylePanel);

    addClickHandler(label, stylePanel, idx++);
  }
};
