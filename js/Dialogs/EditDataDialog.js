/**
 * Constructs a new metadata dialog.
 */
var EditDataDialog = function (ui, cell) {
    var div = document.createElement('div');
    var graph = ui.editor.graph;

    var value = graph.getModel().getValue(cell);

    // Converts the value to an XML node
    if (!mxUtils.isNode(value)) {
        var doc = mxUtils.createXmlDocument();
        var obj = doc.createElement('object');
        obj.setAttribute('label', value || '');
        value = obj;
    }

    // Creates the dialog contents
    var form = new mxForm('properties');
    form.table.style.width = '100%';

    var attrs = value.attributes;
    var names = [];
    var keys = [];
    var texts = [];
    var count = 0;

    var addTextArea = function (index, name, key, value) {
        names[index] = name;
        keys[index] = key;
        texts[index] = form.addTextarea(names[count] + ':', value, 1);
        texts[index].style.width = '100%';
    };

    var temp = [];

    var nodeType = attrs["nodeType"];
    var cost = attrs["klmCost"];
    var label = attrs["label"];

    if (parseInt(nodeType.value) === window.NodeTypeEnum.Custom) {
        temp.push({
            name: "Cost",
            key: cost.name,
            value: cost.nodeValue
        });
    }

    temp.push({
        name: "Label",
        key: label.name,
        value: label.nodeValue
    });

    var combo = form.addCombo(nodeType.name, false, 1);
    // todo make schön
    form.addOption(combo, "Unknown", 0, parseInt(nodeType.nodeValue) == window.NodeTypeEnum.Unknown);
    form.addOption(combo, "Custom", 1, parseInt(nodeType.nodeValue) == window.NodeTypeEnum.Custom);
    form.addOption(combo, "Mouse Button Click", 2, parseInt(nodeType.nodeValue) == window.NodeTypeEnum.ButtonClickNode);

    for (var i = 0; i < temp.length; i++) {
        if (temp[i].key !== "nodeType") {
            addTextArea(count, temp[i].name, temp[i].key, temp[i].value);
        }
        count++;
    }

    var top = document.createElement('div');
    top.style.cssText = 'position:absolute;left:30px;right:30px;overflow-y:auto;top:30px;bottom:80px;';
    top.appendChild(form.table);

    var newProp = document.createElement('div');
    newProp.style.whiteSpace = 'nowrap';
    newProp.style.marginTop = '6px';

    top.appendChild(newProp);
    div.appendChild(top);

    this.init = function () {
        if (texts.length > 0) {
            texts[0].focus();
        }
    };

    var cancelBtn = mxUtils.button(mxResources.get('cancel'), function () {
        ui.hideDialog.apply(ui, arguments);
    });

    cancelBtn.className = 'geBtn';

    var applyBtn = mxUtils.button(mxResources.get('apply'), function () {
        try {
            ui.hideDialog.apply(ui, arguments);

            // Clones and updates the value
            value = value.cloneNode(true);

            for (var i = 0; i < keys.length; i++) {
                if (texts[i] == null) {
                    value.removeAttribute(keys[i]);
                } else {
                    value.setAttribute(keys[i], texts[i].value);
                }
            }

            value.setAttribute("nodeType", combo.value);

            // Updates the value of the cell (undoable)
            graph.getModel().setValue(cell, value);
        } catch (e) {
            mxUtils.alert(e);
        }
    });
    applyBtn.className = 'geBtn gePrimaryBtn';

    var buttons = document.createElement('div');
    buttons.style.cssText = 'position:absolute;left:30px;right:30px;text-align:right;bottom:30px;height:40px;';

    if (ui.editor.cancelFirst) {
        buttons.appendChild(cancelBtn);
        buttons.appendChild(applyBtn);
    } else {
        buttons.appendChild(applyBtn);
        buttons.appendChild(cancelBtn);
    }

    div.appendChild(buttons);
    this.container = div;
};
/**
 * Optional help link.
 */
EditDataDialog.getDisplayValueForCell = function (ui, cell) {
    var name = null;
    if (ui.editor.graph.getModel().getParent(cell) != null) {
        name = cell.getValue();
    }
    return name;
};