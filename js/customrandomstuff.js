// Extends EditorUi to update I/O action states based on availability of backend
(function () {
  mxEvent.disableContextMenu(document.body);
  var editorUiInit = EditorUi.prototype.init;

  EditorUi.prototype.init = function () {
    editorUiInit.apply(this, arguments);
    this.actions.get("export").setEnabled(true);

    // Updates action states which require a backend
    if (!Editor.useLocalStorage) {
      mxUtils.post(OPEN_URL, "", mxUtils.bind(this, function (req) {
        let enabled = req.getStatus() !== 404;
        this.actions.get("open").setEnabled(enabled || Graph.fileSupport);
        this.actions.get("import").setEnabled(enabled || Graph.fileSupport);
        this.actions.get("save").setEnabled(enabled);
        this.actions.get("saveAs").setEnabled(enabled);
        this.actions.get("export").setEnabled(enabled);
      }));
    }
  };

  // Adds required resources (disables loading of fallback properties, this can only
  // be used if we know that all keys are defined in the language specific file)
  mxResources.loadDefaultBundle = false;
  const bundle = mxResources.getDefaultBundle(RESOURCE_BASE, mxLanguage) ||
    mxResources.getSpecialBundle(RESOURCE_BASE, mxLanguage);

  function addStartActivityNode(editorUi) {
    const parent = editorUi.editor.graph.getDefaultParent();
    editorUi.editor.graph.getModel().beginUpdate();
    var xlmDocument = mxUtils.createXmlDocument();
    var node = xlmDocument.createElement('root');
    node.setAttribute('nodeType', window.NodeTypeEnum.Structural);
    node.setAttribute('label', "Root");
    node.setAttribute('statusQuo', 1);

    try {
      editorUi.editor.graph.insertVertex(parent, null, node, 50, 50, 80, 30);
    } finally {
      // Updates the display
      editorUi.editor.graph.getModel().endUpdate();
    }
  }

  // Fixes possible asynchronous requests
  mxUtils.getAll([bundle, STYLE_PATH + "/default.xml"], function (xhr) {
    // Adds bundle text to resources
    mxResources.parse(xhr[0].getText());

    // Configures the default graph theme
    var themes = {};
    themes[Graph.prototype.defaultThemeName] = xhr[1].getDocumentElement();

    // Main
    const main = new EditorUi(new Editor(urlParams["chrome"] === "0", themes));
    main.createMenus();
    addStartActivityNode(main);
  }, function () {
    document.body.innerHTML = "<center style=\"margin-top:10%;\">Error loading resource files. Please check browser console.</center>";
  });
})();

/**
 * Object.prototype.forEach() polyfill
 * https://gomakethings.com/looping-through-objects-with-es6/
 * @author Chris Ferdinandi
 * @license MIT
 */
if (!Object.prototype.forEach) {
  Object.defineProperty(Object.prototype, 'forEach', {
    value: function (callback, thisArg) {
      if (this == null) {
        throw new TypeError('Not an object');
      }
      thisArg = thisArg || window;
      for (var key in this) {
        if (this.hasOwnProperty(key)) {
          callback.call(thisArg, this[key], key, this);
        }
      }
    }
  });
}